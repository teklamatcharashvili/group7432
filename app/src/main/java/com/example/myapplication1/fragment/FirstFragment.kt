package com.example.myapplication1.fragment

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.myapplication1.R

class FirstFragment : Fragment(R.layout.fragment_first) {


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val button = view.findViewById<Button>(R.id.button)
        val amount = view.findViewById<EditText>(R.id.Number)
        val controller = Navigation.findNavController(view)
        button.setOnClickListener {
            val amountt = amount.text.toString()
            if (amountt.isEmpty()){
                return@setOnClickListener
            }

            val amount = amountt.toInt()

            val action = FirstFragmentDirections.actionFirstFragmentToSecondFragment(amount)
            controller.navigate(action)







        }
    }
}